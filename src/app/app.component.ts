import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DataVehicle } from './models/data.module';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  dataContacts = this.formBuilder.group({
    veiculo: ['', [Validators.required]],
    placa: ['', [Validators.required, Validators.minLength(7), Validators.maxLength(7)]],
    motorista: ['', [Validators.required]],
    telefone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(11)]],
    entrada: [''],
    saida: [''],
    total: [0]
  })

  allDatasVehicle: DataVehicle[] = new Array()

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    let list = localStorage.getItem('veihcle-list')
    if (list) {
      this.allDatasVehicle = JSON.parse(list)
    }
  }

  saveData() {
    let veiculo = this.dataContacts.value.veiculo
    let placa = this.dataContacts.value.placa
    let motorista = this.dataContacts.value.motorista
    let telefone = this.dataContacts.value.telefone
    let entrada = this.dataContacts.value.entrada
    let saida = this.dataContacts.value.saida
    let total = this.dataContacts.value.total

    entrada = new Date()

    let data = new DataVehicle(veiculo, placa, motorista, telefone, entrada, saida, total)
    this.allDatasVehicle.push(data)

    localStorage.setItem('veihcle-list', JSON.stringify(this.allDatasVehicle))

    this.dataContacts.reset()
  }

}
