export class DataVehicle {

  public veiculo: string
  public placa: string
  public motorista: string
  public telefone: string
  public entrada: Date = new Date()
  public saida?: Date
  public total: Number

  constructor(
    veiculo: string,
    placa: string,
    motorista: string,
    telefone: string,
    entrada: Date,
    saida: Date,
    total: Number
  ) {
    this.veiculo = veiculo
    this.placa = placa
    this.motorista = motorista
    this.telefone = telefone
    this.entrada = entrada,
      this.saida = saida
    this.total = total
  }

}